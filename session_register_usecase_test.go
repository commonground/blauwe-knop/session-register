// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package sessionregister_test

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"

	"sessionregister"
	"sessionregister/mock"
)

func TestSessionRegisterUseCase_CreateSession(t *testing.T) {
	type fields struct {
		sessionRepository sessionregister.SessionRepository
	}
	type args struct {
		publicKeyPem string
	}

	errCreateSessionFailed := errors.New("ErrCreateSessionFailed")

	sessionTokenString := "mock-session-token"
	mockSessionToken := (*sessionregister.SessionToken)(&sessionTokenString)

	tests := []struct {
		name                 string
		fields               fields
		args                 args
		expectedError        error
		expectedSessionToken *sessionregister.SessionToken
	}{
		{
			name: "session token creation failed",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().CreateSession("dummy-public-key-pem").Return(nil, errCreateSessionFailed).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyPem: "dummy-public-key-pem",
			},
			expectedSessionToken: nil,
			expectedError:        errCreateSessionFailed,
		},
		{
			name: "happy flow",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().CreateSession("dummy-public-key-pem").Return(mockSessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyPem: "dummy-public-key-pem",
			},
			expectedSessionToken: mockSessionToken,
			expectedError:        nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			observedZapCore, _ := observer.New(zap.InfoLevel)
			observedLogger := zap.New(observedZapCore)
			sessionRegisterUserCase := sessionregister.NewSessionRegisterUseCase(observedLogger, tt.fields.sessionRepository)
			sessionToken, err := sessionRegisterUserCase.CreateSession(tt.args.publicKeyPem)
			assert.Equal(t, tt.expectedSessionToken, sessionToken)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestSessionRegisterUseCase_GetSession(t *testing.T) {
	type fields struct {
		sessionRepository sessionregister.SessionRepository
	}
	type args struct {
		sessionToken string
	}

	errGetSessionFailed := errors.New("ErrGetSessionFailed")

	sessionTokenString := "mock-session-token"
	mockSessionToken := (*sessionregister.SessionToken)(&sessionTokenString)

	tests := []struct {
		name                 string
		fields               fields
		args                 args
		expectedError        error
		expectedSessionToken *sessionregister.SessionToken
	}{
		{
			name: "session token creation failed",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().GetSession(sessionTokenString).Return(nil, errGetSessionFailed).AnyTimes()
					return repo
				}(),
			},
			args: args{
				sessionToken: sessionTokenString,
			},
			expectedSessionToken: nil,
			expectedError:        errGetSessionFailed,
		},
		{
			name: "happy flow",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().GetSession(sessionTokenString).Return(mockSessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				sessionToken: sessionTokenString,
			},
			expectedSessionToken: mockSessionToken,
			expectedError:        nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			observedZapCore, _ := observer.New(zap.InfoLevel)
			observedLogger := zap.New(observedZapCore)
			sessionRegisterUserCase := sessionregister.NewSessionRegisterUseCase(observedLogger, tt.fields.sessionRepository)
			sessionToken, err := sessionRegisterUserCase.GetSession(tt.args.sessionToken)
			assert.Equal(t, tt.expectedSessionToken, sessionToken)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestSessionRegisterUseCase_CreateChallenge(t *testing.T) {
	type fields struct {
		sessionRepository sessionregister.SessionRepository
	}
	type args struct {
		publicKeyFingerPrint string
		publicKeyPem         string
	}

	errCreateChallengeFailed := errors.New("ErrCreateChallengeFailed")

	mockChallenge := (*sessionregister.Challenge)(&sessionregister.Challenge{
		PublicKey: "dummy-public-key",
		Random:    "random",
		Date:      time.Now()})

	tests := []struct {
		name              string
		fields            fields
		args              args
		expectedError     error
		expectedChallenge *sessionregister.Challenge
	}{
		{
			name: "session token creation failed",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().CreateChallenge("dummy-public-key-finger-print", "dummy-public-key-pem").Return(nil, errCreateChallengeFailed).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyFingerPrint: "dummy-public-key-finger-print",
				publicKeyPem:         "dummy-public-key-pem",
			},
			expectedChallenge: nil,
			expectedError:     errCreateChallengeFailed,
		},
		{
			name: "happy flow",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().CreateChallenge("dummy-public-key-finger-print", "dummy-public-key-pem").Return(mockChallenge, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyFingerPrint: "dummy-public-key-finger-print",
				publicKeyPem:         "dummy-public-key-pem",
			},
			expectedChallenge: mockChallenge,
			expectedError:     nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			observedZapCore, _ := observer.New(zap.InfoLevel)
			observedLogger := zap.New(observedZapCore)
			sessionRegisterUserCase := sessionregister.NewSessionRegisterUseCase(observedLogger, tt.fields.sessionRepository)
			challenge, err := sessionRegisterUserCase.CreateChallenge(tt.args.publicKeyFingerPrint, tt.args.publicKeyPem)
			assert.Equal(t, tt.expectedChallenge, challenge)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestSessionRegisterUseCase_GetChallenge(t *testing.T) {
	type fields struct {
		sessionRepository sessionregister.SessionRepository
	}
	type args struct {
		publicKeyFingerPrint string
	}

	errGetChallengeFailed := errors.New("ErrGetChallengeFailed")

	mockChallenge := (*sessionregister.Challenge)(&sessionregister.Challenge{
		PublicKey: "dummy-public-key",
		Random:    "random",
		Date:      time.Now()})

	tests := []struct {
		name              string
		fields            fields
		args              args
		expectedError     error
		expectedChallenge *sessionregister.Challenge
	}{
		{
			name: "session token creation failed",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().GetChallenge("dummy-public-key-finger-print").Return(nil, errGetChallengeFailed).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyFingerPrint: "dummy-public-key-finger-print",
			},
			expectedChallenge: nil,
			expectedError:     errGetChallengeFailed,
		},
		{
			name: "happy flow",
			fields: fields{
				sessionRepository: func() sessionregister.SessionRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockSessionRepository(ctrl)
					repo.EXPECT().GetChallenge("dummy-public-key-finger-print").Return(mockChallenge, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				publicKeyFingerPrint: "dummy-public-key-finger-print",
			},
			expectedChallenge: mockChallenge,
			expectedError:     nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			observedZapCore, _ := observer.New(zap.InfoLevel)
			observedLogger := zap.New(observedZapCore)
			sessionRegisterUserCase := sessionregister.NewSessionRegisterUseCase(observedLogger, tt.fields.sessionRepository)
			challenge, err := sessionRegisterUserCase.GetChallenge(tt.args.publicKeyFingerPrint)
			assert.Equal(t, tt.expectedChallenge, challenge)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
