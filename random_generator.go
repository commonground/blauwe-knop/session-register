// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package sessionregister

import (
	"math/rand"
	"time"
)

type RandomGenerator struct {
	letters []rune
}

func NewRandomGenerator() *RandomGenerator {
	rand.Seed(time.Now().Unix())
	return &RandomGenerator{
		letters: []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),
	}
}

func (r *RandomGenerator) generateToken(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = r.letters[rand.Intn(len(r.letters))]
	}
	return string(b)
}

func (r *RandomGenerator) GenerateSessionToken() SessionToken {
	token := r.generateToken(24)
	return SessionToken(token)
}

func (r *RandomGenerator) GenerateChallengeRandom() string {
	return r.generateToken(64)
}
