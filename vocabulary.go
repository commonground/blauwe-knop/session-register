// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package sessionregister

type SessionToken string
