// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package redis

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"time"

	"sessionregister"

	"github.com/go-redis/redis"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SessionRegisterRepository struct {
	client          *redis.Client
	randomGenerator sessionregister.SessionRandomGenerator
}

type SessionRepository interface {
	CreateSession(publicKeyPem string) (*sessionregister.SessionToken, error)
	GetSession(SessionToken string) (*sessionregister.SessionToken, error)
	CreateChallenge(publicKeyFingerprint, publicKeyPEM string) (*sessionregister.Challenge, error)
	GetChallenge(publicKeyFingerPrint string) (*sessionregister.Challenge, error)
	healthcheck.Checker
}

type ChallengeStorageModel struct {
	PublicKeyPEM string    `json:"publicKeyPEM"`
	Random       string    `json:"random"`
	Date         time.Time `json:"date"`
}

type SessionStorageModel struct {
	PublicKeyPEM string    `json:"publicKeyPEM"`
	Date         time.Time `json:"date"`
}

const sessionTokenKeyPrefix = "session-token-"
const challengeKeyPrefix = "challenge-"

func NewSessionRegisterRepository(redisDSN string, randomGenerator sessionregister.SessionRandomGenerator) (*SessionRegisterRepository, error) {
	options, err := redis.ParseURL(redisDSN)
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(options)

	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}
	rand.Seed(time.Now().UnixNano())
	return &SessionRegisterRepository{
		client:          client,
		randomGenerator: randomGenerator,
	}, nil
}

func (s *SessionRegisterRepository) CreateSession(publicKeyPem string) (*sessionregister.SessionToken, error) {
	sessionToken := s.randomGenerator.GenerateSessionToken()

	session := &SessionStorageModel{
		PublicKeyPEM: publicKeyPem,
		Date:         time.Now()}

	jsonBytes, err := json.Marshal(session)
	if err != nil {
		return nil, err
	}
	_, err = s.client.Set(fmt.Sprintf("%s%s", sessionTokenKeyPrefix, string(sessionToken)), string(jsonBytes),
		0).Result()
	if err != nil {
		return nil, err
	}

	return &sessionToken, nil

}

func (s *SessionRegisterRepository) GetSession(sessionToken string) (*sessionregister.SessionToken, error) {
	_, err := s.client.Get(fmt.Sprintf("%s%s", sessionTokenKeyPrefix, sessionToken)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}

	return (*sessionregister.SessionToken)(&sessionToken), nil
}

func (s *SessionRegisterRepository) CreateChallenge(publicKeyFingerprint, publicKeyPem string) (*sessionregister.Challenge, error) {
	challenge := &ChallengeStorageModel{
		PublicKeyPEM: publicKeyPem,
		Random:       s.randomGenerator.GenerateChallengeRandom(),
		Date:         time.Now(),
	}

	jsonBytes, err := json.Marshal(challenge)
	if err != nil {
		return nil, err
	}
	_, err = s.client.Set(fmt.Sprintf("%s%s", challengeKeyPrefix, publicKeyFingerprint), string(jsonBytes), 0).Result()
	if err != nil {
		return nil, err
	}

	return &sessionregister.Challenge{
		PublicKey: publicKeyPem,
		Date:      challenge.Date,
		Random:    challenge.Random,
	}, nil

}

func (s *SessionRegisterRepository) GetChallenge(publicKeyFingerPrint string) (*sessionregister.Challenge, error) {
	valueString, err := s.client.Get(fmt.Sprintf("%s%s", challengeKeyPrefix, publicKeyFingerPrint)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}

		return nil, err
	}

	challengeStorageModel := &ChallengeStorageModel{}

	err = json.Unmarshal([]byte(valueString), challengeStorageModel)
	if err != nil {
		return nil, err
	}

	return &sessionregister.Challenge{
		PublicKey: publicKeyFingerPrint,
		Date:      challengeStorageModel.Date,
		Random:    challengeStorageModel.Random,
	}, nil

}

func (s *SessionRegisterRepository) GetHealthCheck() healthcheck.Result {
	name := "redis"
	status := healthcheck.StatusOK
	start := time.Now()

	pong, err := s.client.Ping().Result()
	if err != nil {
		log.Printf("ping to redis failed: %v", err)
		status = healthcheck.StatusError
	} else {
		log.Printf("ping to redis successful: %s", pong)
	}

	return healthcheck.Result{
		Name:         name,
		Status:       status,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
