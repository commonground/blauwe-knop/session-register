// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"

	"sessionregister"

	http_infra "sessionregister/http"
	"sessionregister/redis"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8084" description:"Address for the session-register api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	RedisDSN      string `long:"redis-dsn" env:"REDIS_DSN" default:"redis://0.0.0.0:6379/0" description:"DSN for the redis database. Read https://pkg.go.dev/github.com/go-redis/redis/v8?tab=doc#ParseURL for more info"`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	sessionRegisterRepository, err := redis.NewSessionRegisterRepository(cliOptions.RedisDSN, sessionregister.NewRandomGenerator())
	if err != nil {
		log.Fatalf("failed to create Redis SessionRegisterRepository: %v", err)
	}

	sessionRegisterUseCase := sessionregister.NewSessionRegisterUseCase(logger, sessionRegisterRepository)
	router := http_infra.NewRouter(sessionRegisterUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
