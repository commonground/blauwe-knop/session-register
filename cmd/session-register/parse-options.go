package main

import (
	"fmt"

	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"
)

func parseOptions() (options, error) {
	var result options
	args, err := flags.Parse(&result)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return result, nil
			}
		}
		return result, fmt.Errorf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		return result, fmt.Errorf("unexpected arguments: %v", args)
	}

	return result, err
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
