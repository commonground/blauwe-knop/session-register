# Use go 1.x based on alpine image.
FROM golang:1.17.3-alpine AS build

ADD . /go/src/session-register/
ENV GO111MODULE on
WORKDIR /go/src/session-register
RUN go mod download
RUN go build -o dist/bin/session-register ./cmd/session-register

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/session-register/dist/bin/session-register /usr/local/bin/session-register

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/session-register"]
