// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package sessionregister

import (
	"errors"
	"time"

	"github.com/coreos/rkt/tests/testutils/logger"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
	"go.uber.org/zap"
)

var ErrPublicKeyIsInvalid = errors.New("public key is invalid")
var ErrSignedMessageInvalid = errors.New("signed message invalid")
var ErrSignatureInvalid = errors.New("signature not signed with the correct private key")

type Challenge struct {
	PublicKey string
	Random    string
	Date      time.Time
}

type SessionRandomGenerator interface {
	GenerateSessionToken() SessionToken
	GenerateChallengeRandom() string
}

type SessionRepository interface {
	CreateSession(publicKeyPem string) (*SessionToken, error)
	GetSession(SessionToken string) (*SessionToken, error)
	CreateChallenge(publicKeyFingerprint, publicKeyPem string) (*Challenge, error)
	GetChallenge(publicKeyFingerPrint string) (*Challenge, error)
	healthcheck.Checker
}

type SessionRegisterUseCase struct {
	sessionRepository SessionRepository
	logger            *zap.Logger
}

func NewSessionRegisterUseCase(logger *zap.Logger, sessionRepository SessionRepository) *SessionRegisterUseCase {
	return &SessionRegisterUseCase{
		sessionRepository: sessionRepository,
		logger:            logger,
	}
}

func (a *SessionRegisterUseCase) CreateSession(publicKeyPem string) (*SessionToken, error) {
	sessionToken, err := a.sessionRepository.CreateSession(publicKeyPem)
	if err != nil {
		logger.Error("unable to create session", zap.Error(err))
		return nil, err
	}

	a.logger.Debug("session created")

	return sessionToken, nil
}

func (a *SessionRegisterUseCase) GetSession(sessionToken string) (*SessionToken, error) {
	sessionTokenResponse, err := a.sessionRepository.GetSession(sessionToken)
	if err != nil {
		return nil, err
	}
	return sessionTokenResponse, nil
}

func (a *SessionRegisterUseCase) CreateChallenge(publicKeyFingerPrint, publicKeyPem string) (*Challenge, error) {
	challenge, err := a.sessionRepository.CreateChallenge(publicKeyFingerPrint, publicKeyPem)
	if err != nil {
		logger.Error("unable to create challenge", zap.Error(err))
		return nil, err
	}

	a.logger.Debug("challenge created", zap.String("random", challenge.Random), zap.Time("date", challenge.Date))

	return challenge, nil
}

func (a *SessionRegisterUseCase) GetChallenge(publicKeyFingerPrint string) (*Challenge, error) {
	challenge, err := a.sessionRepository.GetChallenge(publicKeyFingerPrint)
	if err != nil {
		return nil, err
	}
	return challenge, nil
}

func (a *SessionRegisterUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.sessionRepository,
	}
}
