// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"log"
	"net/http"

	"sessionregister"

	"github.com/go-chi/render"
)

type getChallengePayload struct {
	PublicKeyFingerprint string `json:"publicKeyFingerprint"`
}

type createChallengePayload struct {
	PublicKeyFingerprint string `json:"publicKeyFingerprint"`
	PublicKeyPEM         string `json:"publicKeyPem"`
}

type challengeResponse struct {
	PublicKey string `json:"publicKey"`
	Date      int64  `json:"date"`
	Random    string `json:"random"`
}

func handlerCreateChallenge(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionRegisterUseCaseKey).(*sessionregister.SessionRegisterUseCase)

	requestPayload := &createChallengePayload{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyFingerprint == "" {
		log.Printf("a publicKeyFingerprint is required")
		http.Error(w, "a publicKeyFingerprint is required", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyPEM == "" {
		log.Printf("a public key is required")
		http.Error(w, "a public key is required", http.StatusBadRequest)
		return
	}

	challenge, err := sessionUseCase.CreateChallenge(requestPayload.PublicKeyFingerprint, requestPayload.PublicKeyPEM)
	if err != nil {
		log.Printf("error creating challenge: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(mapChallengeToChallengeResponse(challenge))
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func mapChallengeToChallengeResponse(challenge *sessionregister.Challenge) *challengeResponse {
	return &challengeResponse{
		PublicKey: challenge.PublicKey,
		Date:      challenge.Date.UTC().Unix(),
		Random:    challenge.Random,
	}
}

func handlerGetChallenge(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionRegisterUseCaseKey).(*sessionregister.SessionRegisterUseCase)

	requestPayload := &getChallengePayload{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyFingerprint == "" {
		log.Printf("a public key is required")
		http.Error(w, "a public key is required", http.StatusBadRequest)
		return
	}

	challenge, err := sessionUseCase.GetChallenge(requestPayload.PublicKeyFingerprint)
	if err != nil {
		log.Printf("error creating challenge: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(mapChallengeToChallengeResponse(challenge))
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
