// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"sessionregister"
)

type key int

const (
	sessionRegisterUseCaseKey key = iota
)

func NewRouter(sessionRegisterUseCase *sessionregister.SessionRegisterUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/sessions", func(r chi.Router) {
		r.Post("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionRegisterUseCaseKey, sessionRegisterUseCase)
			handlerCreateSession(w, r.WithContext(ctx))
		})
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionRegisterUseCaseKey, sessionRegisterUseCase)
			handlerGetSession(w, r.WithContext(ctx))
		})
		r.Post("/challenge", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionRegisterUseCaseKey, sessionRegisterUseCase)
			handlerCreateChallenge(w, r.WithContext(ctx))
		})
		r.Get("/challenge", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), sessionRegisterUseCaseKey, sessionRegisterUseCase)
			handlerGetChallenge(w, r.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("session-register", sessionRegisterUseCase.GetHealthChecks())
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
