// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"log"
	"net/http"

	"sessionregister"

	"github.com/go-chi/render"
)

type createSessionPayload struct {
	PublicKeyPEM string `json:"publicKeyPem"`
}

type sessionResponse struct {
	SessionToken sessionregister.SessionToken `json:"sessionToken"`
}

func handlerCreateSession(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionRegisterUseCaseKey).(*sessionregister.SessionRegisterUseCase)

	requestPayload := &createSessionPayload{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.PublicKeyPEM == "" {
		log.Printf("a public key is required")
		http.Error(w, "a public key is required", http.StatusBadRequest)
		return
	}

	session, err := sessionUseCase.CreateSession(requestPayload.PublicKeyPEM)
	if err != nil {
		log.Printf("error creating session: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(mapSessionToSessionResponse(session))
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func mapSessionToSessionResponse(session *sessionregister.SessionToken) *sessionResponse {
	return &sessionResponse{
		SessionToken: *session,
	}
}

func handlerGetSession(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	sessionUseCase, _ := ctx.Value(sessionRegisterUseCaseKey).(*sessionregister.SessionRegisterUseCase)

	requestPayload := &sessionResponse{}
	err := render.DecodeJSON(r.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	if requestPayload.SessionToken == "" {
		log.Printf("a SessionToken is required")
		http.Error(w, "a SessionToken is required", http.StatusBadRequest)
		return
	}

	session, err := sessionUseCase.GetSession((string)(requestPayload.SessionToken))
	if err != nil {
		log.Printf("error creating session: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(mapSessionToSessionResponse(session))
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}
