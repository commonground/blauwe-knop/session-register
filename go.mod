module sessionregister

go 1.17

require go.uber.org/zap v1.19.1

require (
	github.com/jessevdk/go-flags v1.5.0
	gitlab.com/commonground/blauwe-knop/health-checker v0.0.2
)

require (
	github.com/coreos/rkt v1.30.0
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
